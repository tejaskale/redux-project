import axios from 'axios';
import React, { Fragment, useEffect, useRef, useState } from 'react';


const api = axios.create({
  baseURL: 'https://api.themoviedb.org/3/',
});

export const getPopularMovies = () => {
  return api.get('movie/popular', {
    params: {
      api_key: 'ef36e9b88a47da1df8b02235ba46e5c6',
      language: 'en-US',
    }
  });
}

export const getUpcomingMovies = () => {
  return api.get('movie/upcoming/', {
    params: {
      api_key: 'ef36e9b88a47da1df8b02235ba46e5c6',
      language: 'en-US',
    }
  });
}
