import axios from 'axios';
import React, { Fragment, useEffect, useRef, useState } from 'react';
import "./Moviecard.css"
import { getPopularMovies } from '../api';
import {getUpcomingMovies} from "../api";
import SingleMovieCard from '../singleMovieCard/SingleMovieCard';
import { NavLink, useNavigate } from 'react-router-dom';


function Moviecard() {
  const [movies, setMovies] = useState([]);
  const [upcomingMovies, setUpcomingMovies] = useState([]);
  const movieID = [];
  const [isClicked,setIsClicked] = useState(false)

  useEffect(() => {

    getUpcomingMovies().then(response => {
      console.log(response.data.results);
      setUpcomingMovies(response.data.results);
    });

    getPopularMovies().then((response) => {
      console.log(response.data.results);
      setMovies(response.data.results);
    });
  }, []);
  
let navigate = useNavigate()
     
if(isClicked){
  navigate("/movie")
}

  return (
    <Fragment>
        <div className="mcard-container">
        {movies.map(movie => (
        <div className='container' key={movie.id}>
           {movieID.push(movie.id)}
           
          <img className='movie-poster' src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`} alt="" />
          <h2>{movie.title}</h2>
          {/* <p>{movie.overview}</p> */}
          <p>Language : {movie.original_language}</p>
          <h3>Release Date : { movie.release_date} </h3>
         <NavLink to="/movie">
         <p className='more-details' onClick={(e)=>{setIsClicked(true)}} >Click for More Details</p>
         </NavLink>
         </div> 
      ))}
        </div>
        <SingleMovieCard movies={movies} movieID={movieID} />
    </Fragment>
  );
}
export default Moviecard;