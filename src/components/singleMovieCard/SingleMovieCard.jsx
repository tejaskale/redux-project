import React, { Fragment } from 'react';
import "./singleMovie.css"

function SingleMovieCard(props) {

  return (
    <Fragment>
    {
      props.movies.map((movie)=>{
        if(movie.id === props.movieID)
        return (
          <div className='card-container' >
      <div className='movie-card'>
      <div>
        <img src={`https://image.tmdb.org/t/p/w500/${props.movie.poster_path}`} alt="" />
      </div>
      <div>
          <h2>{movie.title}</h2>
          <p>{movie.overview}</p>
          <p>Language : {movie.original_language}</p>
          <h3>Release Date : { movie.release_date} </h3>
          <p className='more-details' >Click for More Details</p>
      </div>
      </div>
    </div>
        )
      })
    }
    </Fragment>
  )
}

export default SingleMovieCard