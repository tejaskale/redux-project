import React, { Fragment } from "react";
import "./aside.css";

function aside() {
  return (
    <Fragment>
      <div className="aside-bar">
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores
          sint dolorem velit nemo magni sapiente neque nostrum libero ipsam,
          adipisci sunt expedita? Nulla soluta assumenda eum nesciunt itaque aut
          suscipit. Quis culpa harum architecto dolorem aperiam nostrum nesciunt
          debitis nulla assumenda. Facere eos optio numquam hic quam,
          reprehenderit rerum repudiandae vitae? Voluptate, nisi sequi commodi
          similique sunt eveniet eos quis. Sequi hic ducimus aut maiores
          aspernatur itaque saepe corporis!
        </p>
        <div className="gener-container">
          <h2 className="movie-gener">Action</h2>
          <h2 className="movie-gener">Comedy</h2>
          <h2 className="movie-gener">Crime</h2>
          <h2 className="movie-gener">Thriller</h2>
          <h2 className="movie-gener">Superhero</h2>
        </div>
      </div>
    </Fragment>
  );
}

export default aside;
