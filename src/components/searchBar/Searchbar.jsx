import React, { Fragment } from 'react';
import './search.css'

function search() {
  return (
    <Fragment>
        <div className='search-bar'>
            <input type="search" placeholder='search movie name' id="" />
            <button>Search</button>
        </div>
    </Fragment>
  )
}

export default search