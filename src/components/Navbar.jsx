import axios from 'axios';
import React, { Fragment, useEffect } from 'react';
import Moviecard from './Moviecard/Moviecard';
import Aside from './aside/Aside';
import Searchbar from './searchBar/Searchbar';
import "./Navbar.css";
import SingleMovieCard from './singleMovieCard/SingleMovieCard';
import { NavLink } from 'react-router-dom';




function Navbar() {

  return (
    <Fragment>
        <div className='navbar'>
           <NavLink to="/">
           <div className='nav-sec1'>
                <img src='https://cdn-icons-png.flaticon.com/512/50/50648.png?w=826&t=st=1674153097~exp=1674153697~hmac=3c1714b458389a1d13bc5d2d012875f67dfdcf6812e5b5530e1f9b55d1f56913' alt="" />
                <h1 className='websiteName'>Movieflix</h1>
            </div>
           </NavLink>
            <Searchbar/>
            <div className='nav-sec2'>
                <p>Movies</p>
                <p>TV Series</p>
                <p>Wbseries</p>
                <p>Help</p>
            </div>
        </div>
        <div className='component-container'>
            <Aside/>
            <Moviecard/>
         
        </div>
    </Fragment>
  )
}

export default Navbar