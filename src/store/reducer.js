import { GET_POPULAR_MOVIES , GET_UPCOMING_MOVIES} from './action';

const initialState = {
  movies: [],
};

const movieReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_POPULAR_MOVIES:
      return {
        ...state,
        movies: action.payload,
      };
      case GET_UPCOMING_MOVIES:
        return {
          ...state,
          movies: action.payload,
        };
    default:
      return state;
  }
};

export default movieReducer;
