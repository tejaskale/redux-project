import { getPopularMovies } from '../components/api';
import {getUpcomingMovies} from "../components/api";


export const GET_POPULAR_MOVIES = 'GET_POPULAR_MOVIES';
export const GET_UPCOMING_MOVIES = 'GET_UPCOMING_MOVIES';


export const fetchPopularMovies = () => {
  return async (dispatch) => {
    const response = await getPopularMovies();
    dispatch({ type: GET_POPULAR_MOVIES, payload: response.data.results });
  };
};

export const fetchUpcomingMovies = () => {
  return async (dispatch) => {
    const response = await getUpcomingMovies();
    dispatch({ type: GET_UPCOMING_MOVIES, payload: response.data.results });
  };
};
