import './App.css';
import { Fragment } from 'react';
import Navbar from './components/Navbar';
import Aside from "./components/aside/Aside";
import Moviecard from './components/Moviecard/Moviecard';
import SingleMovieCard from './components/singleMovieCard/SingleMovieCard';
import { BrowserRouter as Router , Route , Routes } from 'react-router-dom';


function App() {
  return (
    <Fragment>
      <Router>
        <Routes>
          <Route path='/' element={<Navbar/>} />
          <Route path='/movie' element={<SingleMovieCard />} />
        </Routes>
      </Router>
      <Navbar/>
    </Fragment>
  );
}
export default App;
